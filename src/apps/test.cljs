(ns apps.test
  (:require ["serialport" :as SerialPort]
            [link.frame]
            [link.mnp]
            [link.dock]
            [link.session]
            [cljs.core.async :as async]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as timbre :refer [info debug warn error]]))

(defn send-serial [data ^js conn]
  (debug "[serial] <<<" data)
    (->> (data :down)
        (map clj->js)
        (apply #(when-not (nil? %) (.write conn %)))))

(defn handle-data [^js conn data]
  (-> (map identity (js->clj data))
      (link.frame/receive)
      (link.mnp/receive)
      (link.dock/receive)
      (link.session/receive)
      (link.session/send)
      (link.dock/send)
      (link.mnp/send)
      (link.frame/send)
      (send-serial conn)))

(defn connect [port speed]
  (info "Connecting to" port "at" speed "bps")
  (let [^js conn (SerialPort. port #js{:baudRate speed})]
    (.on conn "readable" #(handle-data conn (.read conn)))))

(defn run [args]
  (timbre/set-level!
    (nth [:error :warn :info :debug] (args :verbosity)))
  (connect (args :port) (args :speed)))

(def OPTIONS
  [["-s" "--speed SPEED" "Serial speed" :default 115200]
   ["-p" "--port PORT" "Serial port" :default "/dev/ttyUSB0"]
   ["-v" nil "Log level" :id :verbosity :default 0 :update-fn inc]])

(defn main
  [& args]
  (run (:options (parse-opts args OPTIONS))))
