(ns link.dock-cmds)

(def LONG_DATA "ldta")
(def REF_RESULT "ref ")

;; ----------------------------------------------------------------------
;; Desktop -> Newton

(def QUERY "qury")
(def CURSOR_GOTO_KEY "goto")
(def CURSOR_MAP "cmap")
(def CURSOR_ENTRY "crsr")
(def CURSOR_MOVE "move")
(def CURSOR_NEXT "next")
(def CURSOR_PREV "prev")
(def CURSOR_RESET "rset")
(def CURSOR_RESET_TO_END "rend")
(def CURSOR_COUNT_ENTRIES "cnt ")
(def CURSOR_WHICH_END "whch")
(def CURSOR_FREE "cfre")

;; ----------------------------------------------------------------------
;; Keyboard Passthrough

;; Desktop -> Newton

(def KEYBOARD_CHAR "kbdc")
(def KEYBOARD_STRING "kbds")

;; Desktop -> Newton or Newton -> Desktop
(def START_KEYBOARD_PASSTHROUGH "kybd")

;; ----------------------------------------------------------------------
;; Misc additions

;; Newton -> Desktop Commands

(def DEFAULT_STORE "dfst")
(def APP_NAMES "appn")
(def IMPORT_PARAMETER_SLIP_RESULT "islr")
(def PACKAGE_INFO "pinf")
(def SET_BASE_ID "base")
(def BACKUP_IDS "bids")
(def BACKUP_SOUP_DONE "bsdn")
(def SOUP_NOT_DIRTY "ndir")
(def SYNCHRONIZE "sync")
(def CALL_RESULT "cres")

;; Desktop -> Newton

(def REMOVE_PACKAGE "rmvp")
(def RESULT_STRING "ress")
(def SOURCE_VERSION "sver")
(def ADD_ENTRY_WITH_UNIQUE_ID "auni")
(def GET_PACKAGE_INFO "gpin")
(def GET_DEFAULT_STORE "gdfs")
(def CREATE_DEFAULT_SOUP "cdsp")
(def GET_APP_NAMES "gapp")
(def REG_PROTOCOL_EXTENSION "pext")
(def REMOVE_PROTOCOL_EXTENSION "rpex")
(def SET_STORE_SIGNATURE "ssig")
(def SET_SOUP_SIGNATURE "ssos")
(def IMPORT_PARAMETERS_SLIP "islp")
(def GET_PASSWORD "gpwd")
(def SEND_SOUP "snds")
(def BACKUP_SOUP "bksp")
(def SET_STORE_NAME "ssna")
(def CALL_GLOBAL_FUNCTION "cgfn")
(def CALL_ROOT_METHOD "crmd")
(def SET_VBOCOMPRESSION "cvbo")
(def RESTORE_PATCH "rpat")

;; Desktop -> Newton or Newton -> Desktop

(def OPERATION_DONE "opdn")
(def OPERATION_CANCELED "opcn")
(def OP_CANCELED_ACK "ocaa")
(def REF_TEST "rtst")
(def UNKNOWN_COMMAND "unkn")

;; ----------------------------------------------------------------------
;; Starting a session
;; 2.0 Newton supports a new set of protocols to enhance the connection
;; capabilities. However, since it"s desirable to also support package
;; downloading from NPI, NTK 1.0 and Connection 2.0 the ROMs will also
;; support the old protocol for downloading packages. To make this work
;; the 2.0 ROMs will pretend that they are talking the old protocol when
;; they send the Request_To_Dock message. If a new connection (or other
;;app) is on the other end the protocol will be negotiated up to the
;; current version. Only package loading is supported with the old
;; protocol.

;; Newton -> Desktop Commands

;; data encrypted key
(def PASSWORD "pass")
(def NEWTON_NAME "name")
(def NEWTON_INFO "ninf")

;; Desktop -> Newton Commands

;; Ask Newton to start docking process
(def INITIATE_DOCKING "dock")
;; Info from the desktop (application & so on)
(def DESKTOP_INFO "dinf")
;; Optional to define which icons are shown
(def WHICH_ICONS "wicn")

;; ----------------------------------------------------------------------
;; Sync and Selective Sync

;; Newton -> Desktop Commands

(def REQUEST_TO_SYNC "ssyn")
(def SYNC_OPTIONS "sopt")

;; Desktop -> Newton Commands

(def GET_SYNC_OPTIONS "gsyn")
(def SYNC_RESULTS "sres")
(def SET_STORE_GET_NAMES "ssgn")
(def SET_SOUP_GET_INFO "ssgi")
(def GET_CHANGED_INDEX "cidx")
(def GET_CHANGED_INFO "cinf")

;; ----------------------------------------------------------------------
;; File browsing
;; File browsing will use the same protocol described above with the
;; following additions. For synchronize, the process is completely driven
;; from the desktop side. For file browsing/importing, however, the
;; process is driven from the Newton.

;; Newton -> Desktop Commands

(def REQUEST_TO_BROWSE "rtbr")
;; Windows only
(def GET_DEVICES "gdev")
;; Get the starting path
(def GET_DEFAULT_PATH "dpth")
;; Ask the desktop for files and folders
(def GET_FILES_AND_FOLDERS "gfil")
(def SET_PATH "spth")
(def GET_FILE_INFO "gfin")
(def INTERNAL_STORE "isto")
(def RESOLVE_ALIAS "rali")
;; Windows only
(def GET_FILTERS "gflt")
;; Windows only
(def SET_FILTER "sflt")
;; Windows only
(def SET_DRIVE "sdrv")

;; Desktop -> Newton

;; Windows only
(def DEVICES "devs")
;; Windows only
(def FILTERS "filt")
(def PATH "path")
;; Frame of info about files and folders
(def FILES_AND_FOLDERS "file")
(def FILE_INFO "finf")
(def GET_INTERNAL_STORE "gist")
(def ALIAS_RESOLVED "alir")

;; ----------------------------------------------------------------------
;; No data (?) undocumented.
;; Newton -> Dock
;; Réponse: ocaa
(def OPERATION_CANCELED2 "opca")

;; ----------------------------------------------------------------------
;; File importing
;; File importing uses the file browsing interface described above. After
;; the user taps the import button, the following commands are used.

;; Newton -> Desktop Commands

(def IMPORT_FILE "impt")
(def SET_TRANSLATOR "tran")

;; Desktop -> Newton

(def TRANSLATOR_LIST "trnl")
(def IMPORTING "dimp")
(def SOUPS_CHANGED "schg")
(def SET_STORE_TO_DEFAULT "sdef")

;; ----------------------------------------------------------------------
;; Package loading
;; Package loading uses the file browsing interface described above. After
;; the user taps the load package button, the following commands are used.

;; Newton -> Desktop Commands

(def LOAD_PACKAGE_FILE "lpfl")

;; ----------------------------------------------------------------------
;; Restore originated on Newton
;; Restore uses the file browsing interface described above. After the
;; user taps the restore button, the following commands are used.

;; Newton -> Desktop Commands

(def RESTORE_FILE "rsfl")
(def GET_RESTORE_OPTIONS "grop")
(def RESTORE_ALL "rall")

;; Desktop -> Newton or Newton -> Desktop

(def RESTORE_OPTIONS "ropt")
(def RESTORE_PACKAGE "rpkg")

;; ----------------------------------------------------------------------
;; Desktop Initiated Functions while connected
;; With the advent of the new protocol, the Newton and the desktop can be
;; connected, but with no command specified. A command can be requested by
;; the user on either the Newton or the Desktop. Commands requested by the
;; newton user are discussed above. This section describes the commands
;; sent from the Desktop to the Newton in response to a user request on
;; the desktop.

;; Desktop -> Newton Commands

;; Request_To_Sync "ssyn")
(def REQUEST_TO_RESTORE "rrst")
(def REQUEST_TO_INSTALL "rins")

;; ----------------------------------------------------------------------
;; 1.0 Newton ROM Support

;; Newton -> Desktop Commands

;; Ask PC to start docking process
(def REQUEST_TO_DOCK "rtdk")
;; The name of the newton
;;Newton_Name "name")
;; The current time on the Newton
(def CURRENT_TIME "time")
;; data array of store names & signatures
(def STORE_NAMES "stor")
;; data array of soup names & signatures
(def SOUP_NAMES "soup")
;; data array of ids for the soup
(def SOUP_IDS "sids")
;; data array of ids
(def CHANGED_IDS "cids")
;; data command & result (error)
(def RESULT "dres")
;; data the id of the added entry
(def ADDED_ID "adid")
;; data entry being returned
(def ENTRY "entr")
;; data list of package ids
(def PACKAGE_IDLIST "pids")
;; data package
(def PACKAGE "apkg")
;; data index description array
(def INDEX_DESCRIPTION "indx")
;; data array of class, supperclass pairs 
(def INHERITANCE "dinh")
;; no data
(def PATCHES "patc")

;; Desktop -> Newton

;; data session type
;;Initiate_Docking "dock")
;; The time of the last sync
(def LAST_SYNC_TIME "stme")
;; no data
(def GET_STORE_NAMES "gsto")
;; no data
(def GET_SOUP_NAMES "gets")
;; data store frane
(def SET_CURRENT_STORE "ssto")
;; data soup name
(def SET_CURRENT_SOUP "ssou")
;; no data
(def GET_SOUP_IDS "gids")
;; no date
(def GET_CHANGED_IDS "gcid")
;; data list of IDs
(def DELETE_ENTRIES "dele")
;; data flattened entry
(def ADD_ENTRY "adde")
;; data ID to return
(def RETURN_ENTRY "rete")
;; data ID to return
(def RETURN_CHANGED_ENTRY "rcen")
;; no data
(def EMPTY_SOUP "esou")
;; no data
(def DELETE_SOUP "dsou")
;; data package
(def LOAD_PACKAGE "lpkg")
;; no data
(def GET_PACKAGE_IDS "gpid")
;; no data
(def BACKUP_PACKAGES "bpkg")
;; no data
(def DISCONNECT "disc")
;; no data
(def DELETE_ALL_PACKAGES "dpkg")
;; no data
(def GET_INDEX_DESCRIPTION "gind")
;; data name + index description
(def CREATE_SOUP "csop")
;; no data
(def GET_INHERITANCE "ginh")
;; data ;; of seconds
(def SET_TIMEOUT "stim")
;; no data
(def GET_PATCHES "gpat")
;; no data
(def DELETE_PKG_DIR "dpkd")
;; no data
(def GET_SOUP_INFO "gsin")

;; Desktop -> Newton or Newton -> Desktop

;; data entry being returned
(def CHANGED_ENTRY "cent")
;; variable length data
(def TEST "test")
;; no data
(def HELLO "helo")
;; data soup info frame
(def SOUP_INFO "sinf")
