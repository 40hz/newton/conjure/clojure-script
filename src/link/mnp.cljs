(ns link.mnp
  (:require [tilakone.core :as tk :refer [_]]
            [taoensso.timbre :as timbre :refer [info debug warn error]]))

(def link-request 1)
(def link-disconnect 2)
(def link-transfer 4)
(def link-ack 5)

(def mnp-states
  [{::tk/name :idle
    ::tk/transitions [{::tk/on link-request     ::tk/to :link-request ::tk/actions [:link-request-response]}
                      {::tk/on _}]}

   {::tk/name :link-request
    ::tk/transitions [{::tk/on link-request     ::tk/to :link-request ::tk/actions [:link-request-response]}
                      {::tk/on link-ack         ::tk/to :data-phase}
                      {::tk/on link-disconnect  ::tk/to :idle}
                      {::tk/on link-transfer    ::tk/to :idle}]}
   {::tk/name :data-phase
    ::tk/transitions [{::tk/on link-request     ::tk/to :idle}
                      {::tk/on link-ack         ::tk/to :data-phase   ::tk/actions [:link-acknowledgement]}
                      {::tk/on link-disconnect  ::tk/to :idle         ::tk/actions [:disconnect]}
                      {::tk/on link-transfer    ::tk/to :data-phase   ::tk/actions [:link-transfer]}]}])

(def mnp-process
  {::tk/states mnp-states
   ::tk/state :idle
   ::tk/match? (fn [signal on] (= (signal :type) on))
   ::tk/action! (fn [{::tk/keys [action signal] :as fsm}]
                  (case action
                    :link-request-response (update fsm :down conj [23 1 2 1 6 1 0 0 0 0 255 2 1 2 3 1 8 4 2 64 0 8 1 2])
                    :link-acknowledgement (do (debug "LA") fsm)
                    :link-transfer (update fsm :up conj (-> fsm ::tk/signal :data))
                    :disconnect (do (debug "Disconnect") fsm)))
   :up []
   :down []})

(def mnp-state (atom mnp-process))

(defn get-length [packet]
  (let [l (first packet)]
    (if (= l 255)
      (+ (* 256 (nth packet 1)) (nth packet 2))
      l)))

(defn get-type [packet]
  (if (= (first packet) 255)
    (nth packet 3)
    (second packet)))

(defn decode-link-request [packet]
  (let [offset (if (= (first packet) 255) 2 0)]
    {:length (get-length packet)
     :type (nth packet (+ 1 offset))
     :framing-mode (nth packet (+ 14 offset))
     :max-lt-frames (nth packet (+ 17 offset))
     :max-info-length (+ (nth packet (+ 20 offset)) (* 256 (nth packet (+ 19 offset))))
     :data-phase-opt (nth packet (+ 23 offset))}))

(defn decode-link-acknowledgement [packet]
  {:length (first packet)
   :type (second packet)
   :recv-seq-num (nth packet 2)
   :recv-credit (nth packet 3)})

(defn decode-link-transfer [packet]
  {:length (first packet)
   :type (second packet)
   :recv-credit (nth packet 2)
   :data (drop 3 packet)})

(defn decode-link-disconnect [packet]
  {:length (first packet)
   :type (second packet)
   :reason-code (nth packet 4)
   :user-code (nth packet 7)})

(defn encode-link-acknowledgement [packet]
  [3 5 (:recv-seq-num packet) (:recv-credit packet)])

(defn decode [packet]
  (condp = (get-type packet)
    link-request (decode-link-request packet)
    link-ack (decode-link-acknowledgement packet)
    link-transfer (decode-link-transfer packet)
    link-disconnect (decode-link-disconnect packet)
    {}))

(defn receive [data]
  (debug "[mnp] >>>" data (@mnp-state ::tk/state))
  (let [new-state (->> (map decode (:up data))
                       (reduce tk/apply-signal @mnp-state)
                       (reset! mnp-state))]
    (let [up (select-keys new-state [:up :down])]
      (swap! mnp-state assoc :up [])
      up)))

(defn send [data]
  (debug "[mnp] <<<" data (@mnp-state ::tk/state))
  (swap! mnp-state assoc :down [])
  data)
