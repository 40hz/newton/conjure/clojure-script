(ns link.frame
  (:require [tilakone.core :as tk :refer [_]]
            [link.crc16]
            [taoensso.timbre :as timbre :refer [info debug warn error]]))

(def unframe-states
  [{::tk/name :outside
    ::tk/transitions [{::tk/on 0x16 ::tk/to :syn}
                      {::tk/on _}]}
   {::tk/name :syn
    ::tk/transitions [{::tk/on 0x10 ::tk/to :dle}]}
   {::tk/name :dle
    ::tk/transitions [{::tk/on 0x02 ::tk/to :in-packet}]}
   {::tk/name :in-packet
    ::tk/transitions [{::tk/on 0x10 ::tk/to :dle-in-packet}
                      {::tk/on _    ::tk/to :in-packet  ::tk/actions [:add-to-packet]}]}
   {::tk/name :dle-in-packet
    ::tk/transitions [{::tk/on 0x10 ::tk/to :in-packet  ::tk/actions [:add-to-packet]}
                      {::tk/on 0x03 ::tk/to :etx}]}
   {::tk/name :etx
    ::tk/transitions [{::tk/on _    ::tk/to :crc1       ::tk/actions [:crc1]}]}
   {::tk/name :crc1
    ::tk/transitions [{::tk/on _    ::tk/to :crc2       ::tk/actions [:crc2]}]}
   {::tk/name :crc2
    ::tk/transitions [{::tk/on 0x16 ::tk/to :syn}
                      {::tk/on _    ::tk/to :outside}]}])

(def unframe-process
  {::tk/states unframe-states
   ::tk/state :outside
   ::tk/action! (fn [{::tk/keys [action signal] :as fsm}]
                  (case action
                    :add-to-packet (update fsm :packet #(conj % signal))
                    :crc1 (assoc fsm :crc signal)
                    :crc2 (-> fsm
                              (update :crc #(+ (* 256 %) signal))
                              (update :packets #(conj % (fsm :packet)))
                              (assoc :packet []))))

   :packet []
   :packets []
   :crc 0})

(def parser-state (atom unframe-process))

(defn receive [data]
  (debug "[frame] >>>" data)
  (let [new-state (reduce tk/apply-signal @parser-state data)
        packets (new-state :packets)]
    (reset! parser-state (assoc new-state :packets []))
    {:up packets :down []}))

(defn process-one [output data]
  (-> (if (= data 0x10)
        (update output :data conj data)
        output)
      (update :crc link.crc16/update-crc data)
      (update :data conj data)))

(defn process-frame [data]
  (let [frame (reduce process-one {:crc 0 :data [0x16 0x10 0x02]} data)
        crc (link.crc16/update-crc (frame :crc) 0x03)]
    (-> (frame :data)
        (conj 0x10)
        (conj 0x03)
        (conj (bit-and crc 0xff))
        (conj (bit-shift-right crc 8)))))

(defn send [data]
  (->> (data :down)
       (map process-frame)
       (assoc {} :down)))
