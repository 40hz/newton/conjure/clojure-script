(ns link.dock
  (:require [taoensso.timbre :as timbre :refer [info debug warn error]]))

(defn decode [packet]
  {:cmd (clojure.string/join (map char (take 4 (drop 8 packet))))
   :length (-> (nth packet 15)
               (+ (* 256 (nth packet 14)))
               (+ (* 65536 (nth packet 13)))
               (+ (* 16777216 (nth packet 12))))
   :data (drop 16 packet)})

(defn add-cmd [packet cmd]
  (reduce conj packet (map #(.charCodeAt %) cmd)))

(defn add-length [packet length]
  (-> packet
      (conj (bit-and (bit-shift-right length 24) 0xff))
      (conj (bit-and (bit-shift-right length 16) 0xff))
      (conj (bit-and (bit-shift-right length 8) 0xff))
      (conj (bit-and length 0xff))))

(defn add-data [packet data]
  (reduce conj packet (map identity data)))

(defn encode [packet]
  (-> [110 101 119 116 100 111 99 107]
      (add-cmd (packet :cmd))
      (add-length (count (packet :data)))
      (add-data (packet :data))))

(defn receive [data]
  (debug "[dock] >>>" data)
  (->> data :up
       (map decode)
       (assoc data :up)))

(defn send [data]
  (debug "[dock] <<<" data)
  data)
