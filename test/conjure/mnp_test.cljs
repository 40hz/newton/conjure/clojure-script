(ns conjure.mnp-test
  (:require [link.mnp :as lm]
            [tilakone.core :as tk]
            [cljs.test :refer-macros [deftest is testing run-tests]]))

(def lr-packet [38 1 2 1 6 1 0 0 0 0 255
                2 1 2 3 1 8 4 2 64 0 8 1 3 9 1 1 14 4 3 4 0 250 197 6 1 4 0 0 225 0])
(def lr-packet-long-length [255 0 38 1 2 1 6 1 0 0 0 0 255
                            2 1 2 3 1 8 4 2 64 0 8 1 3 9 1 1 14 4 3 4 0 250 197 6 1 4 0 0 225 0])

(def lr-packet-decoded {:length 38, :type 1, :framing-mode 3,
                        :max-lt-frames 4, :max-info-length 16384, :data-phase-opt 3})
(def la-packet-encoded [3 5 0 8])
(def la-packet-decoded {:length 3 :type 5 :recv-seq-num 0 :recv-credit 8})

(deftest decode-packet
  (testing "Decoding MNP packet"
    (is (= ((lm/decode-link-request lr-packet) lr-packet-decoded)))
    (is (= ((lm/decode-link-request lr-packet-long-length) lr-packet-decoded)))))

(deftest round-trip
  (testing "Round trip encoding"
    (is (= (lm/encode-link-acknowledgement la-packet-decoded) la-packet-encoded))
    (is (= (lm/decode-link-acknowledgement la-packet-encoded) la-packet-decoded))
    (is (= (lm/decode-link-acknowledgement (lm/encode-link-acknowledgement la-packet-decoded))
       la-packet-decoded))
    (is (= (lm/encode-link-acknowledgement (lm/decode-link-acknowledgement la-packet-encoded))
       la-packet-encoded))))

(deftest process-packets
  (testing "Processing packets"
    (prn (link.mnp/receive {:up [lr-packet lr-packet]}))))
