(ns conjure.dock-test
  (:require [link.dock]
            [cljs.test :refer-macros [deftest is testing run-tests]]))

(def request [110 101 119 116 100 111 99 107 114 116 100 107 0 0 0 4 0 0 0 9])

(def response {:cmd "rtdk" :data [0 0 0 0]})

(deftest dock-unpack
  (testing "Unpack dock packet"
    (is (= (link.dock/decode request) {:cmd "rtdk" :length 4 :data [0 0 0 9]}))))

(deftest dock-pack
  (testing "Pack dock packet"
    (is (= (link.dock/encode response) [110 101 119 116 100 111 99 107 114 116 100 107 0 0 0 4 0 0 0 0]))))
