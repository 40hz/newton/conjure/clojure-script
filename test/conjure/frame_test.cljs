(ns conjure.frame-test
  (:require [link.frame :as lf]
            [tilakone.core :as tk]
            [cljs.test :refer-macros [deftest is testing run-tests]]))

(def unframe-data
  [{:data [0x16] :expected :syn}
   {:data [0x16 0x10] :expected :dle}
   {:data [0x16 0x10 0x02] :expected :in-packet}
   {:data [0x16 0x10 0x02 0x01] :expected :in-packet}
   {:data [0x16 0x10 0x02 0x10] :expected :dle-in-packet}
   {:data [0x16 0x10 0x02 0x10 0x10] :expected :in-packet}
   {:data [0x16 0x10 0x02 0x10 0x03] :expected :etx}
   {:data [0x16 0x10 0x02 0x10 0x03 0x55] :expected :crc1}
   {:data [0x16 0x10 0x02 0x10 0x03 0x55 0xaa] :expected :crc2}
   {:data [0x16 0x10 0x02 0x10 0x03 0x55 0xaa] :expected :crc2}
   {:data [0x16 0x10 0x02 0x40 0x10 0x03 0x55 0xaa] :expected :crc2}
   {:data [0x16 0x10 0x02 0x10 0x03 0x55 0xaa 0x16] :expected :syn}
   {:data [0x16 0x10 0x02 0x10 0x03 0x55 0xaa 0x00] :expected :outside}])

(def test-packets
   [{:data [0x16 0x10 0x02 0x40 0x10 0x03 0x55 0xaa] :expected [0x40]}
    {:data [0x16 0x10 0x02 0x40 0x10 0x10 0x10 0x03 0x55 0xaa] :expected [0x40 0x10]}])

(defn process-test-data [data]
  (=
   (data :expected)
   (second (second (reduce tk/apply-signal lf/unframe-process (data :data))))))

(defn process-test-packets [data]
  (=
   (data :expected)
   (first ((reduce tk/apply-signal lf/unframe-process (data :data)) :packets))))

(deftest simple-unframe
  (testing "Simple unframing"
    (is (every? identity (map process-test-data unframe-data)))))

(deftest unframe-packet
  (testing "Unframe packet"
    (is (every? identity (map process-test-packets test-packets)))))

(comment deftest frame-packet
  (testing "Framing packet"
    (prn (lf/process-one {:crc 0 :data []} 0x10))
    (prn (lf/process-frame []))))

(deftest send
  (testing "Sending"
    (prn (lf/send {:down [[0 1][2 3]]}))))
