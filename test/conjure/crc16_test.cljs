(ns conjure.crc16-test
  (:require [link.crc16]
            [cljs.test :refer-macros [deftest is testing run-tests]]))

(defn crc [data]
  (reduce link.crc16/update-crc 0 data))

(deftest crc16
  (testing "CRC16 calculations"
    (is (= (crc []) 0))
    (is (= (crc [0x06 0x00]) 0xa003))
    (is (= (crc [0x03 0x05 0x04 0x07 0x03]) 0x3c47))
    (is (= (crc [0x03 0x05 0x05 0x08 0x03]) 0x0c13))
    (is (= (crc [0x03 0x05 0x04 0x08 0x03]) 0xcc42))))
